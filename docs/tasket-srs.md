# Tasket SRS

A command line app to track your to-dos, tasks, checklists, and other needful things.

## Functional Requirements

Will run where Python runs, on Mac, Windows, Linux, needs 3.6 or later 

Run on the command line, no GUI, no ncurses

Allow for creating, reading, updating, and deleting or archiving tasks

By default, show the current date, calendar, and a list of open tasks at launch

Allow for tasks to persist between sessions, at a user defined location

Show list of tasks after each update to the list

Allow for tagging, or otherwise grouping tasks into projects

Allow for import and export of todo.txt formatted files - see http://todotxt.org/

Installs via pip - 'pip install tasket' - with no further configuration needed

## Constraints

Will not facilitate delegation - sending or triggering emails are out of scope

Will not directly integrate with cloud or cloud storage - API integration is out of scope

Will not sync - out of scope

Will not integrate with groupware or chat apps - out of scope

## Interface requirements

Tasket is a command-line application. It is standalone, so only requires storage to disk. No networking needed beyond initial installation

## Goals

Tasket is intended to be a command line task manager - should facilitate task creation without leaving the coomand line

### 1. Definitions

Task - A unit of work, as definted in the Getting Things Done method by David Allen : https://gettingthingsdone.com/

A task is defined by Tasket as shown by todotxt.org : https://github.com/todotxt/todo.txt

Tasks in tasket will contain all of the items in the todo.txt specification, including the optional items : 

- Completion flag
- Priority field (single letter)
- Completion date
- Creation date 
- Description
- Project Tag
- Context Tag
- Special key value tag (typically a due date, can be something else)

### 2. Workflow

When Tasket is executed with no arguments passed, it will show the date, calendar, and list of current tasks

The list of current tasks will appear with a number in front to allow for further changes, said number can be passed as an argument 

tasket --help will show the instructions

tasket add - this will start the questionnaire to add a new task : 

- Description of task 
- Priority
- Due Date
- Project tag (if any)
- Context tag(if any)

tasket change # - change a task

tasket finish # - mark a task done

#### Future Enhancements

tasket project - show list of projects. This will show a list of prjects known to Tasket with a number in front, said number can be passed as an argument

tasket context - ditto

### 3. Configuration

Tasket will use a database to persist data detween sessions (see schema)

Tasket will use XDG standard (~/.config/tasket/*) for default location of configuration data

Tasket will use this same location for the database, by default

Tasket will allow changes to its config, so that people can put the database in a user defined location. Otherwise, tasket will go back to default settings.

Tasket uses TOML for configuration : https://toml.io/en/

See also: https://github.com/sdispater/tomlkit


## Miscellany

Eventually, the plan is to use Flask for GUI




