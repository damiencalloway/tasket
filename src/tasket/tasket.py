#!/usr/bin/env python

import argparse
import cmd2
import os
import sys

tasklist = []

#Premable - grab the args

class Tasket(cmd2.Cmd):

    #removes cmd2 shell access
    delattr(cmd2.Cmd, 'do_shell')

    parser = cmd2.Cmd2ArgumentParser(description="Maintain a list of needful things to do.")

    parser.add_argument("-t","--task", type=str, help="Add a new task to the list - use single or double quotes")
    parser.add_argument("-d", "--done", type=int, help="Complete a task")
    parser.add_argument("-x", "--skip", type= int, help="Skip a task")
    parser.add_argument("-u", "--update", type= int, help="Update a task")
    parser.add_argument("-r", "--remove", type= int, help="Remove a task")

    @cmd2.with_argparser(parser)

    #Show existing tasks - default state. Cmd2 library uses do* convention as magic method
    def do_list(self, args: argparse.Namespace):
        for i in tasklist:
            print(f'{i}' + ': ' + tasklist[i])

    #Add a task
    def do_add(self, args: argparse.Namespace):
        tasklist.append(task)

    #Complete a task
    def do_complete(self, args: argparse.Namespace):
        print(task)

    #Skip a task
    def do_skip(self, args: argparse.Namespace):
        for i in tasklist:
            print(f'{i}' + ': ' + tasklist[i])

    #Update a task
    def do_update(self, args: argparse.Namespace):
        task = args.task

    #Remove a task
    def do_remove(self, args: argparse.Namespace):
        tasklist.remove(task)



#

#print(17,repr(task))

#Build and maintain the list
    
    

#
    
#

#print(25,repr(tasklist)) 


#Show the current tasks

print('\n')
os.system('date')
print('\n')
os.system('cal')
print('\n')
    

#

#[print(i) for i in tasklist] 

#

#print('\n')

if __name__ == '__main__':
    import sys
    c = Tasket()
    sys.exit(c.cmdloop())


